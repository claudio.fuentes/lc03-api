const express = require('express')
const morgan = require('morgan')
const app = express()
const mysql = require('mysql')
const myConnection = require('express-myconnection')
const userRoute = require('./src/routes/user')
const cors = require('cors')


//server settings
    app.set('port',process.env.PORT || 3012)
    
//
//middleware
    app.use(morgan('dev'))
    app.use(myConnection(mysql,{
        host:'localhost',
        database:'listo_03',
        user:'root',
        port:'3306',
        password:'ToQNGL391#.'
    },'single'))
    app.use(express.urlencoded({extended:"false"}))
    app.use(express.json())
    app.use('/',userRoute)
    app.use(cors())
    
//

app.get('/',(req,res)=>{
    res.send('wake app aplicaciones!')
})
app.get('/Usuario',(req,res)=>{
    res.send(conn.dbCall('CALL spVal_user(?)',["claudio","123"]))
})


app.listen(app.get('port'),()=>{
    console.log('server listening on port ' + app.get('port'));
})